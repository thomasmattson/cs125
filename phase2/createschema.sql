CREATE TABLE manufacturer
(
  id INT NOT NULL,
  name VARCHAR(50) ,
  support_site VARCHAR(100)
);

CREATE TABLE building
(
  id INT NOT NULL,
  campus VARCHAR(50),
  name VARCHAR(50),
  capacity INT
);

CREATE TABLE person
(
  id INT NOT NULL,
  fname VARCHAR(50),
  lname VARCHAR(50),
  phone VARCHAR(11),
  email VARCHAR(50),
  role VARCHAR(50)
);

CREATE TABLE department
(
  id INT NOT NULL,
  name VARCHAR(50),
  head VARCHAR(50),
  division VARCHAR(50)
);

CREATE TABLE server_model
(
  model_id INT NOT NULL,
  cpu INT,
  ram INT,
  os VARCHAR(50),
  hd_size INT,
  virtual VARCHAR(50),
  manuf_id INT NOT NULL
);

CREATE TABLE employee
(
  person_id INT NOT NULL,
  bldg_id INT NOT NULL,
  dept_id INT NOT NULL
);

CREATE TABLE camera_model
(
  recording INT NOT NULL,
  view_range INT NOT NULL,
  resolution INT NOT NULL,
  field_of_view INT NOT NULL,
  model_id INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE projector_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  wi-fi INT NOT NULL,
  lumens INT NOT NULL,
  speakers INT NOT NULL,
  resolution INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE printer_model
(
  model_id INT NOT NULL,
  double_sided INT NOT NULL,
  paper_size INT NOT NULL,
  memory INT NOT NULL,
  speed INT NOT NULL,
  laser INT NOT NULL,
  bw_color INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE computer_model
(
  model_id INT NOT NULL,
  cpu INT NOT NULL,
  screen_size INT NOT NULL,
  resolution INT NOT NULL,
  ram INT NOT NULL,
  os INT NOT NULL,
  type INT NOT NULL,
  hd_size INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE monitor_model
(
  model_id INT NOT NULL,
  hdmi INT NOT NULL,
  panel_type VARCHAR(20) NOT NULL,
  resolution INT NOT NULL,
  size INT NOT NULL,
  refresh_rate INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE nwdevice_models
(
  model_id INT NOT NULL,
  mac_address INT NOT NULL,
  protocol INT NOT NULL,
  type VAR(50) NOT NULL,
  speed INT NOT NULL,
  number_ports INT NOT NULL,
  coverage INT NOT NULL,
  number_clients INT NOT NULL,
  manuf_id INT NOT NULL
);

CREATE TABLE device
(
  model_id INT NOT NULL,
  serial_number BIGINT NOT NULL,
  custom_cpu INT NOT NULL,
  custom_ram INT NOT NULL,
  custom_hdsize INT NOT NULL,
  active VARCHAR(10) NOT NULL,
  year_manufac DATETIME NOT NULL,
  date_purchased DATETIME NOT NULL,
  os INT NOT NULL
);

CREATE TABLE room
(
  room_id INT NOT NULL,
  description VARCHAR(50),
  bldg_id INT NOT NULL
);

CREATE TABLE rack
(
  slot INT NOT NULL,
  rack_id INT NOT NULL,
  room_id INT NOT NULL
);

CREATE TABLE device_owner
(
  serial_number BIGINT NOT NULL,
  person_id INT NOT NULL,
  dept_id INT NOT NULL,
  room_id INT NOT NULL,
  rack_id INT NOT NULL
);
