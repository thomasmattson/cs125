__Thomas Mattson__

- Worked with Adeline Ndacyayisenga, Didi Bulow, and Levi Nelson


---

## Description

> I created a script that takes the sample data from the csv folder and converts it into an SQL query. Everyone else supplied the data for each csv file. I did not enclude the erd file because of no changes to the original diagram.

> To run script:

```
python3 populate.py
```